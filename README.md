# mpv_sponsorblock_lm

Another Sponsorblock implementation for mpv, just a bit less minimal than
[mpv_sponsorblock_minimal](https://codeberg.org/jouni/mpv_sponsorblock_minimal).
`sha256sum` and `curl` are required.

### Supported
- K-Anonymity
- Skip and mute segments
- Full video labels
- Whitelisting channels

### Unsupported
- Point of interest/Highlight segments
- Local database
- Submitting segments or votes

## Configuration

The config file should be placed in `script-opts/sponsorblock_lm.conf` in mpv's
config folder (usually `$XDG_CONFIG_HOME/mpv` or `~/.config/mpv`). Example config:

```
# Enderman, LockPickingLawyer
whitelisted_channels=UCWb-66XSFCV5vgKEbl22R6Q UCm9K6rby98W8JigLoZOh6FQ
find_id_from_title=yes
```

### Available Options
- api_url: URL pointing to the Sponsorblock server's API. (default: `https://sponsor.ajay.app/api`)
- categories: Space-seperated list of [categories](https://wiki.sponsor.ajay.app/w/Types#Category)
  to skip/mute/warn. (default: `sponsor interaction music_offtopic exclusive_access`)
- whitelisted_channels: Space-seperated list of channel IDs to ignore. Sponsor segments will not
  be fetched. Channel IDs are only fetched based on the video's title (see find_id_from_title).
  (default: none)
- find_id_from_filename: Finds video ID from a filename in the form of
  `...-ID.three or four alphanumberic characters`. (default: yes)
- find_id_from_title: Finds video and channel ID from the video title in the form of
  `... (ID by CHANNELID)`. (default: no)

By default, pressing `b` will toggle between enabling and disabling Sponsorblock
(segments will still be fetched) . To bind it to both `b` **and** `s`, add
`s script-binding sponsorblock_lm/toggle` to mpv's `input.conf`. To unbind `b`, add
`b ignore`.
